angular.module('dbconnect.controllers', [])

.controller('ChatCtrl', function (
		$scope, 
		$firebaseObject, 
		$firebaseArray, 
		$timeout, 
		$interval, 
		$ionicScrollDelegate, 
		$cordovaCamera, 
		$sce,
		$ionicLoading
	) {
		
	/*
		// Keep this here in care we wanna put in rooms at some point
	Chats.selectRoom($state.params.roomId);	
	var roomName = Chats.getSelectedRoomName();	
	// Fetching Chat Records only if a Room is Selected
	if (roomName) {
	    $scope.roomName = " - " + roomName;
	    //$scope.chats = Chats.all();
	}
	*/
	
	var viewScroll = $ionicScrollDelegate.$getByHandle('chatsScroll');
  var footerBar;
  var scroller;
  var txtInput;
			
	// Prevents textbox from flying up to the top of the 
	// screen when the keyboard is shown
	if ( window.cordova && window.cordova.plugins.Keyboard ) {
    cordova.plugins.Keyboard.disableScroll( true );
  }
  
  // Scroll up check...
  // If user has scrolled up, we don't want to do the 1 second
  // refresh to scroll in new messages.
  var bottomInView = true;
  $scope.bottomInView = function(index, inview, inviewpart, event) {
		bottomInView = inview;
		return false;
	}	
	
	$interval(function() {
		if ( bottomInView == true ) {  
      viewScroll.scrollBottom(true);
    }  
  }, 500);
  // End scroll up check	
  	
	$scope.$on('$ionicView.loaded', function() {
		
		// Turn on loading indicator (NOTE: don't put this outside of 
		// $ionicView.loaded.  It crashes the app for some reason.
		$ionicLoading.show({
	    noBackdrop: true,
	    template: '<ion-spinner icon="ripple" />'
		});
		
		// Create a Firebase sync object and assign to 
		// $scope.chats	
    $scope.chats = $firebaseArray(fb);
    
    // Turn off loading indicator
    fb.on('value', function () {
	   	$ionicLoading.hide();
		});
	  		
		$scope.IM = {
		  textMessage: ""
		};
		
		$timeout(function() {
      viewScroll.scrollBottom(true);
    }, 3000);
		
		footerBar = document.body.querySelector('#chatsView .bar-footer');
    scroller = document.body.querySelector('#chatsView .scroll-content');
    txtInput = angular.element(footerBar.querySelector('textarea'));
  });
	
		
	$scope.sendMessage = function (msg, isImage) {
		if (isImage != true)
		{
			isImage = false;
		}	    
		$scope.displayName = 'User';
	    
    var chatMessage = {
      from: $scope.displayName,
      message: msg,
      isImage: isImage,
      createdAt: Firebase.ServerValue.TIMESTAMP
    };
		$scope.chats.$add(chatMessage);	    
    $scope.IM.textMessage = "";
    
    // TODO: Make this work, textarea doesn't stay focused
    txtInput.one('blur', function() {
	    txtInput[0].focus();
    });    
    
    $timeout(function() {
      viewScroll.scrollBottom(true);
    }, 1000);
	};
	
	$scope.remove = function (chat) {
	    //Chats.remove(chat);
	}
	
	$scope.sendPhoto = function() {
    var options = { 
        quality : 75, 
        destinationType : Camera.DestinationType.DATA_URL, 
        sourceType : Camera.PictureSourceType.CAMERA, 
        allowEdit : true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      $scope.imgURI = "data:image/jpeg;base64," + imageData;
      $scope.sendMessage( $scope.imgURI, true );
    }, function(err) {
        // An error occured. Show a message to the user
    });
  }
  
  // Resizing textbox in footer bar
  var keyboardHeight = 0;

	window.addEventListener('native.keyboardshow', keyboardShowHandler);
	window.addEventListener('native.keyboardhide', keyboardHideHandler);
	
	function keyboardShowHandler(e) {
	    console.log('Keyboard height is: ' + e.keyboardHeight);
	    keyboardHeight = e.keyboardHeight;
	}
	
	function keyboardHideHandler(e) {
	    console.log('Goodnight, sweet prince');
	    keyboardHeight = 0;
	    $timeout(function() {
	      scroller.style.bottom = footerBar.clientHeight + 'px'; 
	    }, 0);
	}  
  
  $scope.$on('taResize', function(e, ta) {
	  console.log('taResize');
	  if (!ta) return;
	
	  var taHeight = ta[0].offsetHeight;
	  console.log('taHeight: ' + taHeight);
	
	  if (!footerBar) return;
	
	  var newFooterHeight = taHeight + 10;
	  newFooterHeight = (newFooterHeight > 44) ? newFooterHeight : 44;
	
	  footerBar.style.height = newFooterHeight + 'px';
	
	  // for iOS you will need to add the keyboardHeight to the scroller.style.bottom
	  if (device.platform.toLowerCase() === 'ios') {
	    scroller.style.bottom = newFooterHeight + keyboardHeight + 'px'; 
	  } else {
	    scroller.style.bottom = newFooterHeight + 'px'; 
	  }
	});
  
})

// fitlers
.filter('nl2br', ['$filter',
  function($filter) {
    return function(data) {
      if (!data) return data;
      return data.replace(/\n\r?/g, '<br />');
    };
  }
])

// directives
.directive('autoFocus', function($timeout) {
  return {
	  link: function(scope, element, attrs) {
		  $timeout(function() {
			  element[0].focus();
		  }, 150)
	  }
	}
})

.directive('autolinker', ['$timeout',
  function($timeout) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        $timeout(function() {
          var eleHtml = element.html();

          if (eleHtml === '') {
            return false;
          }

          var text = Autolinker.link(eleHtml, {
            className: 'autolinker',
            newWindow: false
          });

          element.html(text);

          var autolinks = element[0].getElementsByClassName('autolinker');

          for (var i = 0; i < autolinks.length; i++) {
            angular.element(autolinks[i]).bind('click', function(e) {
              var href = e.target.href;
              console.log('autolinkClick, href: ' + href);

              if (href) {
	              window.open(href, '_blank');
              }

              e.preventDefault();
              return false;
            });
          }
        }, 0);
      }
    }
  }
])

.directive('ngRandomImage', function () {
    return {
        restrict: 'EA',
        replace: false,
        scope: {
            ngSrc: "="
        },
        link: function (scope, elem, attr) {            
					var random = Math.floor((Math.random()*20)+1);
          elem.attr('src', 'img/avatar-' + random + '.png');
        }
    }
});

// configure moment relative time
moment.locale('en', {
  relativeTime: {
    future: "in %s",
    past: "%s ago",
    s: "a moment",
    m: "a minute",
    mm: "%d minutes",
    h: "an hour",
    hh: "%d hours",
    d: "a day",
    dd: "%d days",
    M: "a month",
    MM: "%d months",
    y: "a year",
    yy: "%d years"
  }
});