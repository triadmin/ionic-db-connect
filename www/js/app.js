var firebaseUrl = "https://dbconnect.firebaseio.com";
var fb = null;

function onDeviceReady() {
    angular.bootstrap(document, ['dbconnect']);
}
//console.log("binding device ready");
// Registering onDeviceReady callback with deviceready event
document.addEventListener('deviceready', onDeviceReady, false);


angular.module('dbconnect', ['ionic', 'dbconnect.controllers', 'angular-inview', 'firebase', 'monospaced.elastic', 'angularMoment', 'ngCordova'])

.run(function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
	  if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    
    // Firebase reference
    fb = new Firebase(firebaseUrl);
    
    // To Resolve Bug
    ionic.Platform.fullScreen();

    $rootScope.firebaseUrl = firebaseUrl;
    $rootScope.displayName = null;
    
    // Hide the status bar
    $cordovaStatusbar.hide();
    
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  
  .state('chats', {
    url: '',
    views: {
        '': {
            templateUrl: 'templates/chats.html',
            controller: 'ChatCtrl',
        }
    }
	}) 
});