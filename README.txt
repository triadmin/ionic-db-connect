DB Connect Ionic App

## Required Cordova plugins: ##
Camera: cordova plugin add org.apache.cordova.camera
Keyboard: cordova plugin add com.ionic.keyboard
Status Bar: cordova plugin add org.apache.cordova.statusbar

## To Build: ##
ionic build ios/android
ionic run ios/android --device

NOTE: Copy files from ~/Resources/icons and ~/Resources/spash to ~/platforms/ios/dbconnect/Resources once you build.  You can then delete them from the project root.